package main

import (
	"fmt"

	"github.com/georgechieng-sc/interns-2022/folders"
	"github.com/gofrs/uuid"
)

func main() {
	req := &folders.FetchFolderRequest{
		OrgID: uuid.FromStringOrNil(folders.DefaultOrgID),
	}
	res, err := folders.GetAllFolders(req)
	if err != nil {
		fmt.Printf("%v", err)
		return
	}
	folders.PrettyPrint(res)
	/*
		req := &folders.FetchFolderRequest{
			OrgID: uuid.FromStringOrNil(folders.DefaultOrgID),
		}
		res, err := folders.GetFolder(req)
		if err != nil {
			fmt.Printf("%v", err)
			return
		}
		folders.PrettyPrint(res)

		res1, err1 := folders.GetNextFolder("11==")
		if err1 != nil {
			fmt.Printf("%v", err1)
			return
		}
		folders.PrettyPrint(res1)
	*/
}
