package folders

import (
	"errors"
	"math/rand"
	"strconv"
	"time"

	"github.com/gofrs/uuid"
)

/*
Global variable to store remaining current folders with the same ID as requested ID
*/
var currentQueue Queue

/*
Ensure that the same token does not get generated twice
*/
var tokenID int

/*
Given FetchFolderRequest req return the first 3 folders that have the same organisation ID as req's OrgID
Argument: req(*FetchFolderRequest) - request folder with the same orgID
Return: *FetchSmallFolderResponse else raise error if request is null or request's OrgId is uuid.Nil
*/

func GetFolder(req *FetchFolderRequest) (*FetchSmallFolderResponse, error) {
	var err error
	if req == nil {
		err = errors.New("FetchFolderRequest is nil\n")
		return nil, err
	}
	if req.OrgID == uuid.Nil {
		err = errors.New("FetchFolderRequest's orgID is nil\n")
		return nil, err
	}
	r, _ := FetchAllFoldersByOrgID(req.OrgID)
	var startToken string
	startToken = GenerateToken()
	var q Queue
	q = Queue{folderArray: r, token: startToken}
	currentQueue = q
	tokenID = 0
	res, _ := GetNextFolder(startToken)
	return res, nil
}

/*
Simple Generated Token
Return: string (2 random character + "==")
*/
func GenerateToken() string {
	rand.Seed(time.Now().UnixNano())
	alphaNum := "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
	var tArray [2]byte
	tArray[0] = alphaNum[rand.Intn(len(alphaNum))]
	tArray[1] = alphaNum[rand.Intn(len(alphaNum))]
	ends := strconv.Itoa(tokenID) + "=="
	tokenID = tokenID + 1
	token := string(tArray[:]) + ends
	return token
}

/*
Get the next 3 or so folders
Arguments: oldToken(string) - token from previous folder response
Return: *FetchSmallFolderResponse if the inputed token is the same as current token
	else if the input is null then return nothing
	else raise error
*/

func GetNextFolder(oldToken string) (*FetchSmallFolderResponse, error) {
	var err error
	if oldToken == currentQueue.token {
		resFolder := []*Folder{}
		numMaxChunk := 3
		folder, numLeft := currentQueue.dequeue()
		for {
			resFolder = append(resFolder, folder)
			numMaxChunk = numMaxChunk - 1
			if numLeft == 0 || numMaxChunk == 0 {
				break
			}
			folder, numLeft = currentQueue.dequeue()
		}
		var newToken string
		if numLeft == 0 {
			newToken = "null"
		} else {
			newToken = GenerateToken()
		}
		currentQueue.token = newToken
		var res *FetchSmallFolderResponse
		res = &FetchSmallFolderResponse{Folders: resFolder, Token: newToken}
		return res, nil
	} else if oldToken == "null" {
		return nil, nil
	} else {
		err = errors.New("Incorrect Token\n")
		return nil, err
	}
}
