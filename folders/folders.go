package folders

import (
	"errors"

	"github.com/gofrs/uuid"
)

/*
	Given FetchFolderRequest
	Exctract OrgID and get array of folders with the same organisation ID from FetchAllFoldersByOrgID
	Two for loops convert a pointer of array of folders (r) to an array of folders' pointers (f) and then back to a pointer of array of folders  (fp)
	Then the pointer of array of folders fp to convert to FetchFolderResponse data structure
*/

/*
Suggest improvement:

	err, f1, fs is declare but not used. It should be deleted because it just create more clutter.
	Or use err to catch error
	Same for k and k1. It should be replace with underscore instead
	the two for loop is redundant as it convert the array pointer to an array and then back again.

	func GetAllFolders(req *FetchFolderRequest) (*FetchFolderResponse, error) {
		var (
			err error
			f1  Folder
			fs  []*Folder
		)
		f := []Folder{}
		r, _ := FetchAllFoldersByOrgID(req.OrgID)
		for k, v := range r {
			f = append(f, *v)
		}
		var fp []*Folder
		for k1, v1 := range f {
			fp = append(fp, &v1)
		}
		var ffr *FetchFolderResponse
		ffr = &FetchFolderResponse{Folders: fp}
		return ffr, nil
	}
	Suggested change below:
*/
func GetAllFolders(req *FetchFolderRequest) (*FetchFolderResponse, error) {
	//suggested changes
	var err error
	if req == nil {
		err = errors.New("FetchFolderRequest is nil\n")
		return nil, err
	}
	if req.OrgID == uuid.Nil {
		err = errors.New("FetchFolderRequest's orgID is nil\n")
		return nil, err
	}
	r, _ := FetchAllFoldersByOrgID(req.OrgID)
	var ffr *FetchFolderResponse
	ffr = &FetchFolderResponse{Folders: r}
	return ffr, nil
}

/*
Return a array of folders with the same organisation ID as given orgID
*/
func FetchAllFoldersByOrgID(orgID uuid.UUID) ([]*Folder, error) {
	folders := GetSampleData()

	resFolder := []*Folder{}
	for _, folder := range folders {
		if folder.OrgId == orgID {
			resFolder = append(resFolder, folder)
		}
	}
	return resFolder, nil
}
