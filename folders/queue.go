package folders

type Queue struct {
	folderArray []*Folder
	token       string
}

func (q *Queue) addQ(x *Folder) {
	q.folderArray = append(q.folderArray, x)
}

func (q *Queue) dequeue() (*Folder, int) {
	if q.folderArray == nil {
		return nil, 0
	}
	var head *Folder
	oldQ := q.folderArray
	length := len(oldQ)
	if length == 1 {
		head = oldQ[0]
		q.folderArray = nil
	} else {
		head, q.folderArray = oldQ[0], oldQ[1:length]
	}
	return head, length - 1
}

func (q *Queue) newQueue(newQ []*Folder) {
	q.folderArray = newQ
}
