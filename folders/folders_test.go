package folders_test

import (
	"testing"

	"github.com/georgechieng-sc/interns-2022/folders"
	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
)

func Test_GetAllFolders(t *testing.T) {
	t.Run("test non existing OrgId", func(t *testing.T) {
		assert := assert.New(t)
		idNotExist := uuid.FromStringOrNil("aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa")
		req := &folders.FetchFolderRequest{
			OrgID: idNotExist,
		}
		res, _ := folders.GetAllFolders(req)
		result := 0
		assert.Equal(len(res.Folders), result, "There should be zero folder for invalid Id")
	})
	t.Run("test null FetchfolderReport", func(t *testing.T) {
		res, err := folders.GetAllFolders(nil)
		println(err)
		if err == nil {
			t.Fail()
			t.Log("Expected error alert")
		}
		if res != nil {
			t.Fail()
			t.Log("Expected nil folders")
		}
	})
	t.Run("test null orgID", func(t *testing.T) {
		req := &folders.FetchFolderRequest{
			OrgID: uuid.Nil,
		}
		res, err := folders.GetAllFolders(req)
		if err == nil {
			t.Fail()
			t.Log("Expected error alert")
		}
		if res != nil {
			t.Fail()
			t.Log("Expected nil folders")
		}
	})
}
